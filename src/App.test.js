import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders App component and checks for footer content', () => {
  render(<App />);
  const linkElement = screen.getByText(/Reza Banazadeh/i);
  expect(linkElement).toBeInTheDocument();
});
