const routes = Object.freeze({
  Home: {
    name: 'Home',
    path: '/',
  },
  SupportForm: {
    name: 'SupportForm',
    path: '/support-form',
  },
  SupportFormSuccess: {
    name: 'SupportFormSuccess',
    path: '/support-form/success',
  },
  SupportFormFailed: {
    name: 'SupportFormFailed',
    path: '/support-form/failed',
  },
});

export default routes;
