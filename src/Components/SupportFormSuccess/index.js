import React from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Table } from 'antd';
import './index.scss';

// no need for translation because it is a mock route
const Success = (props) => {
  const { t } = useTranslation();
  const { location: { state: { formData = false } = {} } = {} } = useHistory();
  return (
    <div className="support-form-success">
      <h1>{t('SupportForm.Success.Title')}</h1>
      <hr />
      <Table
        columns={[
          {
            title: t('SupportForm.Success.FieldNames'),
            dataIndex: 'name',
            key: 'name',
          },
          {
            title: t('SupportForm.Success.FieldValues'),
            dataIndex: 'value',
            key: 'value',
          },
        ]}
        dataSource={Object.keys(formData).map((fieldName, index) => {
          return {
            key: String(index),
            name: fieldName,
            value: formData[fieldName],
          };
        })}
        pagination={false}
      />
    </div>
  );
};

export default Success;
