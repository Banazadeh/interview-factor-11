import React from 'react';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import './index.scss';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const Loader = () => <Spin className="loader" indicator={antIcon} />;

export default Loader;
