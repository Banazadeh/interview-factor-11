import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '../../Helpers/matchMedia.mock.js';
// import the mock before the component
import SupportForm from './index';

const mockLogin = jest.fn((email, password) => {
  return Promise.resolve({ email, password });
});

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (key) => key }),
}));

describe('SupportForm', () => {
  beforeEach(() => {
    render(<SupportForm />);
  });

  it('should display required error when value is not given', async () => {
    fireEvent.click(screen.getByRole('button', { name: /submit/i }));

    // we have 4 fields
    expect(await screen.findAllByRole('alert')).toHaveLength(4);
  });
});
