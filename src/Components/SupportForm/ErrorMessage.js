import React from 'react';
import { useTranslation } from 'react-i18next';
import { ErrorTypes, ErrorIntlKeys, FormFields } from './constants';

export default function ErrorMessage({ name, error = false }) {
  const { t } = useTranslation();
  if (error) {
    const { type } = error;
    let displayMessage = '';
    switch (type) {
      case ErrorTypes.required:
        displayMessage = t(ErrorIntlKeys.required);
        break;
      case ErrorTypes.pattern:
        switch (name) {
          case FormFields.name:
            // only possible if name is empty string
            displayMessage = t(ErrorIntlKeys.required);
            break;
          case FormFields.description:
            // only possible if name is empty string
            displayMessage = t(ErrorIntlKeys.required);
            break;
          case FormFields.email:
            displayMessage = t(ErrorIntlKeys.email);
            break;
          case FormFields.version:
            displayMessage = t(ErrorIntlKeys.version);
            break;
          case FormFields.tel:
            displayMessage = t(ErrorIntlKeys.tel);
            break;
          default:
            console.error('pattern error for missing field');
            return null;
        }
        break;
      default:
        return null;
    }
    return (
      <p className="error-message" role="alert">
        {displayMessage}
      </p>
    );
  }
  return null;
}
