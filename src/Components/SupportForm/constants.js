import { ValidationRegEx } from '../../Helpers/validation';

export const ErrorTypes = Object.freeze({
  required: 'required',
  pattern: 'pattern',
});

export const FormFields = Object.freeze({
  name: 'name',
  email: 'email',
  topic: 'topic',
  topicGeneral: 'topicGeneral',
  topicSoftwareError: 'topicSoftwareError',
  topicRecall: 'topicRecall',
  version: 'version',
  version1: 'version1',
  version2: 'version2',
  tel: 'tel',
  description: 'description',
});

export const ErrorIntlKeys = Object.freeze({
  required: `SupportForm.Validation.${ErrorTypes.required}`,
  email: `SupportForm.Validation.${ErrorTypes.pattern}.${FormFields.email}`,
  version: `SupportForm.Validation.${ErrorTypes.pattern}.${FormFields.version}`,
  tel: `SupportForm.Validation.${ErrorTypes.pattern}.${FormFields.tel}`,
});

export const FormIntlKeys = Object.freeze({
  title: 'SupportForm.title',
  submit: 'SupportForm.submit',
  name: `SupportForm.Fields.${FormFields.name}`,
  namePlaceholder: `SupportForm.Fields.${FormFields.name}.placeholder`,
  email: `SupportForm.Fields.${FormFields.email}`,
  emailPlaceholder: `SupportForm.Fields.${FormFields.email}.placeholder`,
  topic: `SupportForm.Fields.${FormFields.topic}`,
  topicPlaceholder: `SupportForm.Fields.${FormFields.topic}.placeholder`,
  topicGeneral: `SupportForm.Fields.${FormFields.topicGeneral}`,
  topicSoftwareError: `SupportForm.Fields.${FormFields.topicSoftwareError}`,
  topicRecall: `SupportForm.Fields.${FormFields.topicRecall}`,
  version: `SupportForm.Fields.${FormFields.version}`,
  versionPlaceholder: `SupportForm.Fields.${FormFields.version}.placeholder`,
  tel: `SupportForm.Fields.${FormFields.tel}`,
  telPlaceholder: `SupportForm.Fields.${FormFields.tel}.placeholder`,
  description: `SupportForm.Fields.${FormFields.description}`,
  descriptionPlaceholder: `SupportForm.Fields.${FormFields.description}.placeholder`,
});

export const ValidationPattern = Object.freeze({
  // negative lookahead to empty string
  name: /^(?! ).*$/,
  description: /^(?! ).*$/,
  email: ValidationRegEx.email,
  // easy regex for version number. No constrains regarding the length were given
  // [FormFields.version]: /^\d+\.\d+\.\d+$/, // no use because we split the input field in 3 fields
  tel: ValidationRegEx.tel,
});
