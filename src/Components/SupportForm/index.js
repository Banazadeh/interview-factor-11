import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { Input, Select, Button, Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import routes from '../../routes';
import { FormIntlKeys, FormFields, ValidationPattern } from './constants';
import FormField from './FormField';
import ErrorMessage from './ErrorMessage';
import './index.scss';

const SupportForm = (props) => {
  const { t } = useTranslation();
  const {
    handleSubmit,
    errors,
    control,
    watch,
    formState: { isSubmitting },
  } = useForm({ required: true, mode: 'onBlur' });
  const history = useHistory();
  const onSubmit = useCallback((data) => {
    try {
      // pass data to external validation service (be)
      console.log('data in onSubmit: ', data);
      history.push(routes.SupportFormSuccess.path, { formData: data });
    } catch (error) {
      // code should never reach here
      history.push(routes.SupportFormFailed.path, { formData: data, error });
    }
  });
  const { [FormFields.topic]: topic } = watch();
  return (
    <div className="support-form">
      <Row id="title-row">
        <Col span={24}>
          <h1 className="title">{t(FormIntlKeys.title)}</h1>
        </Col>
      </Row>
      <hr />
      <Row>
        <Col xs={24} sm={11}>
          <FormField
            control={control}
            name={FormFields.name}
            placeholder={t(FormIntlKeys.namePlaceholder)}
            label={t(FormIntlKeys.name)}
            as={Input}
            errors={errors}
            rules={{ required: true, pattern: ValidationPattern.name }}
            defaultValue={null}
          />
        </Col>

        <Col xs={24} sm={{ span: 11, offset: 2 }}>
          <FormField
            control={control}
            name={FormFields.email}
            placeholder={t(FormIntlKeys.emailPlaceholder)}
            label={t(FormIntlKeys.email)}
            as={Input}
            errors={errors}
            rules={{ required: true, pattern: ValidationPattern.email }}
            type="email"
            defaultValue={null}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={11}>
          <FormField
            control={control}
            name={FormFields.topic}
            placeholder={t(FormIntlKeys.topicPlaceholder)}
            label={t(FormIntlKeys.topic)}
            as={
              <Select>
                <Select.Option value={FormFields.topicGeneral}>
                  {t(FormIntlKeys.topicGeneral)}
                </Select.Option>
                <Select.Option value={FormFields.topicSoftwareError}>
                  {t(FormIntlKeys.topicSoftwareError)}
                </Select.Option>
                <Select.Option value={FormFields.topicRecall}>
                  {t(FormIntlKeys.topicRecall)}
                </Select.Option>
              </Select>
            }
            errors={errors}
            rules={{ required: true }}
          />

          {topic === FormFields.topicRecall && (
            <FormField
              control={control}
              name={FormFields.tel}
              placeholder={t(FormIntlKeys.telPlaceholder)}
              label={t(FormIntlKeys.tel)}
              as={Input}
              errors={errors}
              type="tel"
              rules={{ required: true, pattern: ValidationPattern.tel }}
              defaultValue={null}
            />
          )}
          {topic === FormFields.topicSoftwareError && (
            <div className="field-group">
              <label htmlFor={FormFields.version}>
                {t(FormIntlKeys.version)}:
              </label>
              <div id="version">
                <Controller
                  control={control}
                  as={Input}
                  name={FormFields.version}
                  placeholder={t(FormIntlKeys.versionPlaceholder).split('.')[0]}
                  rules={{ required: true, pattern: /^\d+$/, min: 0 }}
                  defaultValue={null}
                />
                <b>.</b>
                <Controller
                  control={control}
                  as={Input}
                  name={FormFields.version1}
                  placeholder={t(FormIntlKeys.versionPlaceholder).split('.')[1]}
                  rules={{ required: true, pattern: /^\d+$/, min: 0 }}
                  defaultValue={null}
                />
                <b>.</b>
                <Controller
                  control={control}
                  as={Input}
                  name={FormFields.version2}
                  placeholder={t(FormIntlKeys.versionPlaceholder).split('.')[2]}
                  rules={{ required: true, pattern: /^\d+$/, min: 0 }}
                  defaultValue={null}
                />
              </div>
              <ErrorMessage
                name={FormFields.version}
                error={
                  errors[FormFields.version] ||
                  errors[FormFields.version1] ||
                  errors[FormFields.version2]
                }
              />
            </div>
          )}
        </Col>

        <Col xs={24} sm={{ span: 11, offset: 2 }}>
          <FormField
            id="description"
            control={control}
            name={FormFields.description}
            placeholder={t(FormIntlKeys.descriptionPlaceholder)}
            label={t(FormIntlKeys.description)}
            as={Input.TextArea}
            errors={errors}
            type="text"
            rules={{ required: true, pattern: ValidationPattern.description }}
            defaultValue={null}
          />
        </Col>
      </Row>
      <hr />
      <Row id="submit-row">
        <Col xs={24} sm={12} md={6}>
          <Button
            disabled={isSubmitting}
            onClick={handleSubmit(onSubmit)}
            name="submit"
          >
            {t(FormIntlKeys.submit)}
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default SupportForm;
