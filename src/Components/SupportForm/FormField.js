import React from 'react';
import { Controller } from 'react-hook-form';
import ErrorMessage from './ErrorMessage';

const FormField = ({
  control,
  name,
  placeholder,
  label,
  rules,
  as,
  type,
  errors,
  defaultValue,
  id,
  className,
  ...restProps
}) => (
  <div className="field-group">
    <label htmlFor={name}>{label}:</label>
    <Controller
      {...{
        control,
        as,
        name,
        placeholder,
        type,
        rules,
        defaultValue,
        id,
        className,
        ...restProps,
      }}
    />
    <ErrorMessage name={name} error={errors[name]} />
  </div>
);
export default React.memo(FormField);
