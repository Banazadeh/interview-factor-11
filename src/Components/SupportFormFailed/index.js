import React from 'react';
import './index.scss';

// no need for translation because it is a mock route
const Failed = (props) => (
  <div className="support-form-failed">
    <h1>Something went wrong</h1>
  </div>
);

export default Failed;
