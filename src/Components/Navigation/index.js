import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import { Layout, Menu, Select } from 'antd';
import routes from '../../routes';
import { languages, defaultLanguage } from '../../i18n';
import './index.scss';

const Navigation = (props) => {
  const { pathname } = useLocation();
  // determine the menu item which should be highlighted as selected by antd
  // the highlighting should be synchronous to the route
  const defaultSelectedKeys = useMemo(() => {
    switch (pathname) {
      case routes.Home.path:
        return ['1'];
      case routes.SupportForm.path:
        return ['2'];
      default:
        // let the props for Menu component be unset
        return undefined;
    }
  }, [pathname]);
  const [t, i18n] = useTranslation();
  // only reinitialize the function when i18n reference changes
  const changeLng = useCallback(
    (language) => {
      // set new language key in localStorage
      localStorage.setItem('lng', language);
      i18n.changeLanguage(language);
    },
    [i18n]
  );
  return (
    <Layout.Header className="header">
      <div className="menu">
        <Menu mode="horizontal" defaultSelectedKeys={defaultSelectedKeys}>
          <Menu.Item key="1">
            <Link to={routes.Home.path}>
              {t(`Navigation.Link.${routes.Home.name}`)}
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to={routes.SupportForm.path}>
              {t(`Navigation.Link.${routes.SupportForm.name}`)}
            </Link>
          </Menu.Item>
        </Menu>
      </div>
      <div className="language-selection">
        <Select
          defaultValue={defaultLanguage}
          onChange={(value) => changeLng(value)}
          style={{ background: 'blue' }}
        >
          {Object.values(languages).map((l) => (
            <Select.Option key={l.key} value={l.key}>
              {l.flag} {t(`Navigation.Lng.${l.name}`)}
            </Select.Option>
          ))}
        </Select>
      </div>
    </Layout.Header>
  );
};

export default Navigation;
