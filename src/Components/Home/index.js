import React from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Button } from 'antd';
import routes from '../../routes';
import './index.scss';

const Home = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  return (
    <div className="home">
      <h1>{t('Home.Title')}</h1>
      <div className="content">
        <p>{t('Home.SubTitle')}</p>

        <p>{t('Home.Text')}</p>
      </div>
    </div>
  );
};

export default Home;
