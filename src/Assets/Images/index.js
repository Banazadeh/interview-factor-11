import React from 'react';
import flagGerman from './flag-germany.png';
import flagEnglish from './flag-united-kingdom.png';

export const flagImgGerman = <img className="flag" src={flagGerman} alt="🇩🇪" />;
export const flagImgEnglish = (
  <img className="flag" src={flagEnglish} alt="🇬🇧" />
);
