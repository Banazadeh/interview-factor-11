import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';
import { useTranslation } from 'react-i18next';
import Loader from './Components/Loader';
import Navigation from './Components/Navigation';
import routes from './routes';
import { author, email } from './constants';
import './App.scss';

const { Content, Footer } = Layout;

// modulizing the routes and therefore leveraging code splitting
const Modules = Object.freeze({
  [routes.Home.name]: lazy(() => import('./Components/Home')),
  [routes.SupportForm.name]: lazy(() => import('./Components/SupportForm')),
  [routes.SupportFormSuccess.name]: lazy(() =>
    import('./Components/SupportFormSuccess')
  ),
  [routes.SupportFormFailed.name]: lazy(() =>
    import('./Components/SupportFormFailed')
  ),
});

function App() {
  const { t } = useTranslation();
  return (
    <div className="App">
      <Router>
        <Layout className="layout">
          <Suspense fallback={<Loader />}>
            <Navigation />
            <Content className="content">
              <Switch>
                <Route exact path={routes.Home.path}>
                  <Modules.Home />
                </Route>
                <Route exact path={routes.SupportForm.path}>
                  <Modules.SupportForm />
                </Route>
                <Route path={routes.SupportFormSuccess.path}>
                  <Modules.SupportFormSuccess />
                </Route>
                <Route path={routes.SupportFormFailed.path}>
                  <Modules.SupportFormFailed />
                </Route>
                <Route path="*">404</Route>
              </Switch>
            </Content>
            <Footer>
              {t('Footer.About')} <a href={`mailto:${email}`}>{author}</a>
            </Footer>
          </Suspense>
        </Layout>
      </Router>
    </div>
  );
}

export default App;
