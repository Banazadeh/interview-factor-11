export const ValidationRegEx = Object.freeze({
  // https://html.spec.whatwg.org/multipage/input.html#email-state-(type=email)
  // implementation of the w3 definition of type=email used in html5 forms
  email: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
  // https://html.spec.whatwg.org/multipage/input.html#telephone-state-(type=tel)
  // explains why tel typed fields are still format free. So we will allow number sequences with an optional + in the beginning
  // we allow '+' in front for the country code and numbers only which can be separated by spaces
  tel: /^\+?(\d ?)+\d$/,
});
