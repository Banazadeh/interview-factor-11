import React from 'react';
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import { flagImgEnglish, flagImgGerman } from './Assets/Images';
import {
  FormIntlKeys,
  ErrorIntlKeys,
} from './Components/SupportForm/constants';

export const languages = {
  german: {
    name: 'German',
    flag: flagImgGerman,
    key: 'de',
  },
  english: {
    name: 'English',
    flag: flagImgEnglish,
    key: 'en',
  },
};

export const defaultLanguage = (() => {
  const storedLng = localStorage.getItem('lng');
  const defaultLng = languages.german.key;
  const lngKeys = Object.values(languages).map((lngObject) => lngObject.key);
  // either use the stored language key if it is valid
  if (lngKeys.includes(storedLng)) return storedLng;
  // or return the default language key and save it in localStorage
  localStorage.setItem('lng', defaultLng);
  return defaultLng;
})();

i18next.use(initReactI18next).init({
  lng: defaultLanguage,
  keySeparator: false,
  resources: {
    [languages.german.key]: {
      translation: {
        'Navigation.Link.Home': 'Start',
        'Navigation.Link.SupportForm': 'Supportformular',
        [`Navigation.Lng.${languages.german.name}`]: 'Deutsch',
        [`Navigation.Lng.${languages.english.name}`]: 'Englisch',

        [ErrorIntlKeys.required]: 'Dieses Feld muss ausgefüllt sein',
        [ErrorIntlKeys.email]: 'Sie müssen eine gültige E-Mail eintragen',
        [ErrorIntlKeys.version]:
          'Hier dürfen nur gültige Versions-Nummern folgender Form eingetragen werden: ZAHL.ZAHL.ZAHL',
        [ErrorIntlKeys.tel]: 'Sie müssen eine gültige Telefonnummer eintragen',

        [FormIntlKeys.title]: 'Supportformular',
        [FormIntlKeys.submit]: 'Abschicken',
        [FormIntlKeys.name]: 'Name',
        [FormIntlKeys.namePlaceholder]: 'Max Mustermann',
        [FormIntlKeys.email]: 'Emailadresse',
        [FormIntlKeys.emailPlaceholder]: 'mustermann@email.de',
        [FormIntlKeys.topic]: 'Themenbereich',
        [FormIntlKeys.topicPlaceholder]: 'Bitte wählen',
        [FormIntlKeys.topicGeneral]: 'Allgemeine Anfrage',
        [FormIntlKeys.topicSoftwareError]: 'Softwarefehler',
        [FormIntlKeys.topicRecall]: 'Rückruf',
        [FormIntlKeys.version]: 'Versionsnummer',
        [FormIntlKeys.versionPlaceholder]: '12.123.23',
        [FormIntlKeys.tel]: 'Telefonnummer',
        [FormIntlKeys.telPlaceholder]: '0123456789',
        [FormIntlKeys.description]: 'Beschreibung',
        [FormIntlKeys.descriptionPlaceholder]:
          'Bitte beschreiben Sie Ihr Anliegen',

        'SupportForm.Success.Title': 'Erfolg',
        'SupportForm.Success.FieldNames': 'Name des Feldes',
        'SupportForm.Success.FieldValues': 'Eingetragener Wert',

        'Home.Title': 'Startseite',
        'Home.SubTitle': 'Willkommen in der ganz besonderen Supportform:',
        'Home.Text':
          'Wir werden versuchen ihnen so schnell wie möglich unter die Arme zu greifen. Danke für Ihre Geduld.',

        'Footer.About': 'Interview Test für Factor11 - Erstellt von',
        test: 'test auf deutsch',
      },
    },
    [languages.english.key]: {
      translation: {
        'Navigation.Link.Home': 'Home',
        'Navigation.Link.SupportForm': 'Support form',
        [`Navigation.Lng.${languages.german.name}`]: 'German',
        [`Navigation.Lng.${languages.english.name}`]: 'English',

        [ErrorIntlKeys.required]: 'This is required',
        [ErrorIntlKeys.email]: 'You need to enter a valid email',
        [ErrorIntlKeys.version]:
          'Only version numbers of NUMBER.NUMBER.NUMBER form are valid',
        [ErrorIntlKeys.tel]: 'You need to enter a valid phone number',

        [FormIntlKeys.title]: 'Support form',
        [FormIntlKeys.submit]: 'Submit',
        [FormIntlKeys.name]: 'Name',
        [FormIntlKeys.namePlaceholder]: 'John Doe',
        [FormIntlKeys.email]: 'Email address',
        [FormIntlKeys.emailPlaceholder]: 'example@email.com',
        [FormIntlKeys.topic]: 'Topic',
        [FormIntlKeys.topicPlaceholder]: 'Please select',
        [FormIntlKeys.topicGeneral]: 'General request',
        [FormIntlKeys.topicSoftwareError]: 'Software error',
        [FormIntlKeys.topicRecall]: 'Recall',
        [FormIntlKeys.version]: 'Version number',
        [FormIntlKeys.versionPlaceholder]: '12.123.23',
        [FormIntlKeys.tel]: 'Phone number',
        [FormIntlKeys.telPlaceholder]: '0123456789',
        [FormIntlKeys.description]: 'Description',
        [FormIntlKeys.descriptionPlaceholder]: 'Please describe your request',

        'SupportForm.Success.Title': 'Success',
        'SupportForm.Success.FieldNames': 'Name of the field',
        'SupportForm.Success.FieldValues': 'Value of the field',

        'Home.Title': 'Homepage',
        'Home.SubTitle': 'Welcome to the somewhat special support form:',
        'Home.Text':
          'We will do our best to help you as soon as possible to endeavor your problems. Thank you for your patience.',

        'Footer.About': 'Interview Test for Factor11 - Created by',
        test: 'test on english',
      },
    },
  },
});
